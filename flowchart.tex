% Some useful formatting macros
\newcommand{\code}[1]{\texttt{#1}}
\definecolor{purple}{RGB}{128,0,128}

% Define block styles
\tikzstyle{label} = [font=\scriptsize,
                     midway]
\tikzstyle{entrypoint} = [rectangle,
                          draw,
                          fill=yellow!20,
                          text centered,
                          minimum height=2em]
\tikzstyle{decision} = [diamond,
                        draw,
                        fill=blue!20,
                        text width=6.5em,
                        text badly centered,
                        inner sep=0pt]
\tikzstyle{block} = [rectangle,
                     draw,
                     fill=blue!20,
                     text width=8em,
                     text centered,
                     rounded corners,
                     minimum height=4em]
\tikzstyle{special-block} = [rectangle,
                             draw,
                             fill=purple!50,
                             text width=8em,
                             text centered,
                             rounded corners,
                             minimum height=4em]
\tikzstyle{item} = [rectangle,
                    draw,
                    fill=yellow!20,
                    text width=8em,
                    text centered,
                    minimum height=2em]
\tikzstyle{object} = [rectangle,
                      draw,
                      fill=green!20,
                      font=\scriptsize,
                      text centered]
\tikzstyle{line} = [draw,
                    -latex']
\tikzstyle{cloud} = [draw,
                     ellipse,
                     fill=red!20,
                     minimum height=2em]
\tikzstyle{stack} = [draw,
                     rectangle split,
                     rectangle split parts=#1,
                     anchor=center]
\tikzstyle{object-stack} = [draw,
                            rectangle split,
                            rectangle split parts=#1,
                            anchor=center,
                            fill=green!20,
                            font=\scriptsize]

\begin{tikzpicture}
    \node [entrypoint] (coala-entrypoint) {coala};

    % coala works with multiple categories of "settings":
    % - settings: affect coala's internal behaviour, e.g. file- and bear-loading.
    % - aspects: define possible lint-types, like code-redundancy or whitespacing.
    % - aspect options: options that are bound to a certain aspect and represent the degrees of freedom available for that aspect.

    \node [block, right=of coala-entrypoint] (load-settings) {load settings, aspects \& aspect options};
    \path [line] (coala-entrypoint) -- (load-settings);

        \node [block, right=of load-settings] (merge-settings) {merge settings, aspects \& aspect options};
        \path [line] (load-settings) -- (merge-settings);

        \node [item, right=of merge-settings] (user-coafile) {user coafile};
        \node [item, above=0.25cm of user-coafile] (system-coafile) {system coafile};
        \node [item, below=0.25cm of user-coafile] (project-coafile) {project coafile};
        \path [line] (merge-settings) -- (system-coafile.west);
        \path [line] (system-coafile) -- (user-coafile);
        \path [line] (user-coafile) -- (project-coafile);
        \path [line] (project-coafile.west) -- (merge-settings);

    \node [block, below=of load-settings] (loading-files) {loading files};
    \path [line] (merge-settings.south) -- +(0, -0.5cm) -| (loading-files);

        \node [object-stack=3, right=of loading-files] (file-factories) {
            \nodepart{one}\code{FileFactory}
            \nodepart{two}\code{FileFactory}
            \nodepart{three}\ldots
        };
        \draw [dashed, ->] (loading-files) -- (file-factories);

    \node [block, below=of loading-files] (detect-language) {detect file language(s)};
    \path [line] (loading-files) -- (detect-language);

        \node [object-stack=3, left=of detect-language] (language-mapping) {
            \nodepart{one}\code{FileFactory} $\rightarrow$ \code{[lang1, lang2, \ldots]}
            \nodepart{two}\code{FileFactory} $\rightarrow$ \code{[lang1, lang2, \ldots]}
            \nodepart{three}\ldots
        };
        \draw [dashed, ->] (detect-language) -- (language-mapping);

        \node [block, right=of detect-language, text width=12em] (language-overrides) {match user-specified language override patterns (from section) to file paths};
        \path [line] (detect-language) -- (language-overrides);

        \node [block, right=of language-overrides] (incorporate-language-overrides) {incorporate overrides};
        \path [line] (language-overrides) -- (incorporate-language-overrides);

        \node [block, right=3cm of incorporate-language-overrides] (isolate-file-endings) {isolate file endings};
        \draw [->] (incorporate-language-overrides) -- (isolate-file-endings) node [label, above] {remaining files};

        \node [block, right=of isolate-file-endings, text width=12em] (map-file-endings-to-languages) {map file endings to possible languages/dialects};
        \path [line] (isolate-file-endings) -- (map-file-endings-to-languages);

    \node [block, below=2cm of detect-language] (lil-preprocessing) {Language-in-Language (LIL) pre-processing};
    \path [line] (detect-language) -- (lil-preprocessing);

        \node [block, right=of lil-preprocessing] (get-language-for-lil) {Get language(s) for each file};
        \path [line] (lil-preprocessing) -- (get-language-for-lil);
        \draw [dashed, ->] (language-mapping) |- ($ (get-language-for-lil.north) + (0, 1cm) $) -- (get-language-for-lil);

        \node [decision, right=2cm of get-language-for-lil] (contains-other-languages?) {Contains other languages?};
        \draw [->] (get-language-for-lil) -- (contains-other-languages?) node [label, above] {language};

        \node [block, right=of contains-other-languages?] (run-lil-extractor) {Run respective LIL-extractor};
        \draw [->] (contains-other-languages?) -- (run-lil-extractor) node [label, above] {yes};

        \node [block, right=of run-lil-extractor] (extract-lil) {Extract LIL into temporary files};
        \path [line] (run-lil-extractor) -- (extract-lil);
        \draw [->] (extract-lil) -- +(0, -2cm) -| (get-language-for-lil) node [label, below] {files};
        \draw [dashed, ->] (extract-lil) -- +(8cm, 0) |- (file-factories);

    \node [block, below=4cm of lil-preprocessing] (loading-bears) {loading bears};
    \path [line] (lil-preprocessing) -- (loading-bears);

        \node [object-stack=3, left=of loading-bears] (bears) {
            \nodepart{one}\code{Bear}
            \nodepart{two}\code{Bear}
            \nodepart{three}\ldots
        };
        \draw [dashed, ->] (loading-bears) -- (bears);

        \node [block, right=of loading-bears, text width=10em] (load-bears-from-coala-bears-module) {load all bears registered in \code{coala.bears} module};
        \path [line] (loading-bears) -- (load-bears-from-coala-bears-module);

        \node [block, right=of load-bears-from-coala-bears-module, text width=12em] (incorporate-bear-overrides) {incorporate user-specified aspect+language$\rightarrow$bear overrides specified in section};
        \path [line] (load-bears-from-coala-bears-module) -- (incorporate-bear-overrides);

        \node [block, right=5cm of incorporate-bear-overrides, text width=18em] (select-bears) {
            select bears where \ldots
            \begin{itemize}
                \item aspects map to user-selected ones in section
                \item languages are a superset of represented languages in loaded files
            \end{itemize}
        };
        \draw [->] (incorporate-bear-overrides) -- (select-bears) node [label, above] {remaining aspects \& languages};
        \draw [dashed, ->] (language-mapping) |- ($ (select-bears.north) + (0, 1cm) $) -- (select-bears);

        \node [decision, right=of select-bears] (all-aspects-and-languages-covered?) {all specified aspects and languages covered?};
        \path [line] (select-bears) -- (all-aspects-and-languages-covered?);

            \node [block, above=of all-aspects-and-languages-covered?] (flag-missing-aspects-and-languages) {flag missing aspect+language combinations};
            \draw [->] (all-aspects-and-languages-covered?) -- (flag-missing-aspects-and-languages) node [label, left] {no};

            \node [special-block, right=of flag-missing-aspects-and-languages] (exit-coala) {exit coala};
            \path [line] (flag-missing-aspects-and-languages) -- (exit-coala);

        \node [decision, right=of all-aspects-and-languages-covered?] (bear-conflicts?) {conflicts?\\\footnotesize{(more than one selected bear maps to the same aspect)}};
        \draw [->] (all-aspects-and-languages-covered?) -- (bear-conflicts?) node [label, above] {yes};

        \node [block, right=of bear-conflicts?, text width=12em] (ask-user-for-preferred-bear) {ask user to pick preferred bear for each language+aspect conflict};
        \draw [->] (bear-conflicts?) -- (ask-user-for-preferred-bear) node [label, above] {yes};

        \node [decision, right=of ask-user-for-preferred-bear] (save-flag-specified?) {\code{--save} CLI flag specified?};
        \path [line] (ask-user-for-preferred-bear) -- (save-flag-specified?);

        \node [block, right=of save-flag-specified?] (save-bear-overrides) {save choices to section as bear-overrides};
        \draw [->] (save-flag-specified?) -- (save-bear-overrides) node [label, above] {yes};

    \node [block, below=4cm of loading-bears] (loading-cache) {loading cache};
    \draw [->] (bear-conflicts?.south) -- +(0, -1cm) node [label, right] {no} |- ($ (loading-cache.north) + (0, 1cm) $) -- (loading-cache);
    \draw [->] (save-flag-specified?.south) -- +(0, -1cm) node [label, right] {no} |- ($ (loading-cache.north) + (0, 1cm) $) -- (loading-cache);
    \draw [->] (save-bear-overrides.south) -- +(0, -1cm) |- ($ (loading-cache.north) + (0, 1cm) $) -- (loading-cache);

        \node [object, right=of loading-cache] (cache) {\code{Cache}};
        \draw [dashed, ->] (loading-cache) -- (cache);

    \node [block, below=8cm of loading-cache] (running-core) {running core};
    \path [line] (loading-cache) -- (running-core);

        \node [block, right=of running-core, text width=12em] (initialize-dependencies) {initialize dependencies and check circular references};
        \path [line] (running-core) -- (initialize-dependencies);
        \draw [dashed, ->] (initialize-dependencies) -- +(0, 2.5cm) -| (bears);

        \node [object, below=of initialize-dependencies] (dependency-tracker) {\code{DependencyTracker}};
        \draw [dashed, ->] (initialize-dependencies) -- (dependency-tracker);

        \node [block, right=of initialize-dependencies] (schedule-bears) {schedule bears};
        \path [line] (initialize-dependencies) -- (schedule-bears);

        \node [block, right=of schedule-bears, text width=12em] (offload-tasks) {offload tasks from \texttt{bear.generate\_tasks()}};
        \path [line] (schedule-bears) -- (offload-tasks);

        \node [special-block, right=of offload-tasks] (wait-for-task) {wait for any task to complete};
        \path [line] (offload-tasks) -- (wait-for-task);

        \node [decision, right=3cm of wait-for-task] (cache-available?) {cache available?};
        \draw [->] (wait-for-task) -- (cache-available?) node [label, above] {results available};

        \node [decision, above right=2cm of cache-available?.north east] (task-cached?) {task cached?};
        \draw [->] (cache-available?) |- (task-cached?) node [label, above] {yes};

            \node [block, above right=2cm of task-cached?.north east] (retrieve-results-from-cache) {retrieve results from cache};
            \draw [->] (task-cached?) |- (retrieve-results-from-cache) node [label, above] {yes};


            \node [block, right=of task-cached?] (schedule-task) {schedule task};
            \draw [->] (task-cached?) -- (schedule-task) node [label, above] {no};
            \draw [->] (cache-available?) -| (schedule-task) node [label, below] {no};

            \node [decision, right=of schedule-task] (cache-available?2) {cache available?};
            \path [line] (schedule-task) -- (cache-available?2);

            \node [block, right=of cache-available?2] (update-cache) {update cache};
            \draw [->] (cache-available?2) -- (update-cache) node [label, above] {yes};

        \node [block, right=of update-cache, text width=14em] (set-bear-dependencies) {set \code{bear.dependency\_results} of dependent bears};
        \draw [->] (cache-available?2.south) -- +(0, -1cm) node [label, left] {no} -| (set-bear-dependencies);
        \path [line] (update-cache) -- (set-bear-dependencies);
        \path [line] (retrieve-results-from-cache) -| (set-bear-dependencies);

        \node [decision, right=of set-bear-dependencies] (all-tasks-done?) {all tasks of bear done?};
        \path [line] (set-bear-dependencies) -- (all-tasks-done?);

            \node [block, right=of all-tasks-done?] (resolve-dependent-tasks) {resolve dependent bears and their tasks};
            \draw [->] (all-tasks-done?) -- (resolve-dependent-tasks) node [label, above] {yes};

        \node [decision, right=of resolve-dependent-tasks] (resolved-tasks-available?) {resolved tasks available?};
        \path [line] (resolve-dependent-tasks) -- (resolved-tasks-available?);

        \node [block, right=of resolved-tasks-available?] (schedule-resolved-tasks) {schedule resolved tasks};
        \draw [->] (resolved-tasks-available?) -- (schedule-resolved-tasks) node [label, above] {yes};

        \node [block, right=of schedule-resolved-tasks] (call-result-callback) {call result callback};
        \path [line] (schedule-resolved-tasks) -- (call-result-callback);

        \node [decision, right=of call-result-callback] (more-tasks?) {more tasks?};
        \path [line] (call-result-callback) -- (more-tasks?);
        \draw [->] (resolved-tasks-available?.north) -- +(0, 1cm) node [label, left] {no} -| (call-result-callback);
        \draw [->] (all-tasks-done?.north) -- +(0, 1cm) node [label, left] {no} -| (call-result-callback);
        \draw [->] (more-tasks?.north) -- +(0, 3cm) node [label, right] {yes} -| (wait-for-task);

    \node [block, below=2cm of running-core] (result-handling) {result handling};
    \path [line] (call-result-callback) |- (result-handling);

    \node [block, below=3cm of result-handling] (result-ignore-check) {check whether result is ignored};
    \path [line] (result-handling) -- (result-ignore-check);

        \node [block, right=of result-ignore-check, text width=16em] (last-scan-position-less-affected-code-position) {\code{result.affected\_code.position\\>\\last\_scan\_position}};
        \path [line] (result-ignore-check) -- (last-scan-position-less-affected-code-position);

        \node [block, right=of last-scan-position-less-affected-code-position, text width=16em] (scan-file-for-ignore-ranges) {scan \code{result.affected\_code.file} for ignore-markers from last remembered position in file until \code{result.affected\_code.position}};
        \draw [->] (last-scan-position-less-affected-code-position) -- (scan-file-for-ignore-ranges) node [label, above] {yes};

        \node [object, above=of scan-file-for-ignore-ranges] (last-scan-position) {\code{last\_scan\_position(file)} (initially \code{0})};
        \draw [dashed, ->] (last-scan-position) -- (scan-file-for-ignore-ranges);
        \draw [dashed, ->] (last-scan-position.west) -- (last-scan-position-less-affected-code-position);

        \node [block, right=of scan-file-for-ignore-ranges, text width=12em] (cache-ignore-ranges) {put scanned ignore-ranges and \code{last\_scan\_position} into ignore-cache};
        \path [line] (scan-file-for-ignore-ranges) -- (cache-ignore-ranges);

        \node [object, above=of cache-ignore-ranges] (ignore-cache) {\code{IgnoreCache}};
        \draw [dashed, ->] (cache-ignore-ranges) -- (ignore-cache);
        \draw [dashed, ->] (ignore-cache) -- (last-scan-position);

        \node [decision, right=of cache-ignore-ranges] (result-ignored?) {result ignored?};
        \path [line] (cache-ignore-ranges) |- (result-ignored?);
        \draw [->] (last-scan-position-less-affected-code-position) -- +(0, 3cm) node [label, left] {no} -| (result-ignored?);
        \draw [dashed, ->] (ignore-cache.east) -- (result-ignored?);

        \node [block, right=of result-ignored?] (result-ignored) {done};
        \path [line] (result-ignored?) -- (result-ignored) node [label, above] {yes};

    \node [block, below=2cm of result-ignore-check] (display-results) {display results}; % Only CLI  -> Print results and execute actions.
    \path [line] (result-ignored?) -- +(0, -2cm) node [label, left] {no} -| (display-results);

    \node [block, below=of display-results] (handle-actions) {handle actions};
    \path [line] (display-results) -- (handle-actions);

        \node [block, right=of handle-actions, text width=10em] (present-actions) {present actions\\(via \code{str(action)})};
        \path [line] (handle-actions) -- (present-actions);

        \node [special-block, right=of present-actions] (user-chooses-action) {user chooses action};
        \path [line] (present-actions) -- (user-chooses-action);

        \node [decision, right=of user-chooses-action] (no-action-chosen?) {do nothing?};
        \path [line] (user-chooses-action) -- (no-action-chosen?);

        \node [block, right=of no-action-chosen?] (apply-action) {\code{action.apply()}};
        \draw [->] (no-action-chosen?) -- (apply-action) node [label, above] {no};

        \draw [->] (apply-action) -- +(0, 2cm) -| (present-actions);

        \node [block, below=of no-action-chosen?] (result-handled) {done};
        \draw [->] (no-action-chosen?) -- (result-handled) node [label, left] {yes};

    \node [block, below=6cm of handle-actions] (lil-postprocessing) {Language-in-Language (LIL) post-processing};
    \draw [->] (more-tasks?.east) -- +(1cm, 0) node [label, above] {no} |- ($ (lil-postprocessing.north) + (0, 1cm) $) -- (lil-postprocessing);

        \node [block, right=of lil-postprocessing, text width=12em] (recursive-back-copy) {Recursive back-copy into original files (to support patching LIL-contents)};
        \path [line] (lil-postprocessing) -- (recursive-back-copy);

        \node [block, right=of recursive-back-copy] (delete-lil-tempfiles) {Delete LIL-temporary-files};
        \path [line] (recursive-back-copy) -- (delete-lil-tempfiles);

    \node [block, below=of lil-postprocessing] (cache-postprocessing) {cache post-processing};
    \path [line] (delete-lil-tempfiles) -- +(0, -1.25cm) -| (cache-postprocessing);

        \node [block, right=of cache-postprocessing] (cache-eviction) {cache eviction\\(LRU)};
        \path [line] (cache-postprocessing) -- (cache-eviction);

        \node [block, right=of cache-eviction] (pickle-cache) {\code{pickle.dump()}};
        \path [line] (cache-eviction) -- (pickle-cache);

        \node [block, right=of pickle-cache, text width=12em] (cache-optimization) {cache optimization (\code{pickletools.optimize})};
        \path [line] (pickle-cache) -- (cache-optimization);

    \node [block, below=of cache-postprocessing] (persist-cache) {persist cache to disk};
    \path [line] (cache-optimization) -- +(0, -1.25cm) -| (persist-cache);

\end{tikzpicture}
