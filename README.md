# Functional Structure-Graph of coala

Written in LaTeX.

## Generate PDF

```bash
pdflatex main.tex
```

If you don't want to install LaTeX locally, you can use a docker-image. See
https://github.com/Makman2/texlive-docker.

> Beware: The texlive-docker image is pretty large (about 3.5GB!) and building
> takes quite long.
